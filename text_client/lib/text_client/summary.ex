defmodule TextClient.Summary do
  alias TextClient.State

  def display(game = %{tally: tally}) do
    IO.puts [
      "\n",
      "Word so far:  #{Enum.join(tally.letters, " ")}\n",
      "Guesses left: #{tally.turns_left}\n",
     ]
    game
  end

  def answer(%State{game_service: gs}) do
    gs |> :sys.get_state() |> display_word
  end

  defp display_word(%{letters: letters}) do
    letters |> Enum.join("")
  end

end
